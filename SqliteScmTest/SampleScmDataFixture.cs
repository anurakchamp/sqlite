using System;
using System.Data.Common;                                       // System.Data.Common defines common data-access types.
using Microsoft.Data.Sqlite;

namespace SqliteScmTest
{
    public class SampleScmDataFixture : IDisposable               // The connection object is disposable, so the fixture should dispose of it.
    {
        private const string PartTypeTable = @"CREATE TABLE PartType(
               Id INTEGER PRIMARY KEY,
               Name VARCHAR(255) NOT NULL
            );";

        private const string InventoryItemTable =
            @"CREATE TABLE InventoryItem(
            PartTypeId INTEGER PRIMARY KEY,
            Count INTEGER NOT NULL,
            OrderThreshold INTEGER,
            FOREIGN KEY(PartTypeId) REFERENCES PartType(Id)
            );";

        public SqliteConnection Connection { get; private set; }

        public SampleScmDataFixture()
        {
            var conn = new SqliteConnection("Data Source=:memory:");
            Connection = conn;
            conn.Open();

            var command = new SqliteCommand(PartTypeTable, conn);
            command.ExecuteNonQuery();
            command = new SqliteCommand(InventoryItemTable, conn);
            command.ExecuteNonQuery();
            command = new SqliteCommand(
            @"INSERT INTO PartType
               (Id, Name)
               VALUES
               (0, '8289 L-shaped plate')",
            conn);
            command.ExecuteNonQuery();

            command = new SqliteCommand(
            @"INSERT INTO InventoryItem
                (PartTypeId, Count, OrderThreshold)
                VALUES
                (0, 100, 10)",
            conn);
            command.ExecuteNonQuery();
        }

        public void Dispose()
        {
            if (Connection != null)
                Connection.Dispose();
        }
    }
}