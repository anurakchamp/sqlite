namespace WidgetScmDapper
{
    public class InventoryType
    {
        public int PartTypeId { get; set; }
        public int Count { get; set; }
        public int OrderThreshold { get; set; }
    }
}