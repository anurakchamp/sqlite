using System.Data.Common;
using Dapper;

namespace WidgetScmDapper
{
    public class ScmContext
    {
        private DbConnection connection;

        public IEnumerable<PartType> Parts { get; private set; }
        public IEnumerable<InventoryType> Inventory { get; private set; }

        public ScmContext(DbConnection conn)
        {
            connection = conn;
            Parts = conn.Query<PartType>("SELECT * FROM PartType");
            Inventory = conn.Query<InventoryType>("SELECT * FROM InventoryItem");
        }
    }
}